#!/usr/bin/python3

#fitting ana params

import re,sys,pdb,math, subprocess
import numpy as np
import matplotlib.pyplot as plt


def d_instantTurnOnSpot(th):
    global v_1, v_2, w_1, w_2, t_1, t_2, omega, g, b_brake, a_2

    tau = t_1 + (w_1+w_2)/(2*v_1)    #time taken for agent1 to leave the corridor
   
    if not b_brake or (t_2>tau):     #escape distance
        d_2 = v_2 * tau
        d_esc = d_2
        #print(d_esc)
        return d_esc

    else:                            #crash distance

        t_stop = t_2 + v_2/(a_2)     #time since start that agent2 stops completely
        if tau<t_stop:        #agent2 passes agent1 before stopping
            d_2 = v_2*tau - 0.5*a_2*(tau-t_2)**2
            d_crash=d_2
        else:                   #agent2 stops before reachign agent1
            d_2 = v_2*t_2 + v_2**2/(2*a_2)
            d_1 = 0 #TODO

            pdb.set_trace()
            y_1 = v_1 * t_stop
            h = (w_1+w_2)/2
            d_1 = math.sqrt(h**2 - y_1**2)    #giving imaginary number?


            d_crash=d_2 + d_1
            #print(d_crash)
        return d_crash




def d_turnOnSpotThenStraight(th):
    global v_1, v_2, w_1, w_2, t_1, t_2, omega, g, b_brake, a_2
    if th<0:
        th=-th

    tau = t_1 + (w_1 + w_2)/(2*v_1) + (abs(math.pi/2 - abs(th)))/omega     #time for Agent1 to leave corridor 

    if not b_brake or (t_2>tau):     #escape distance
        d_2 = v_2*tau
        d_esc = d_2
        return d_esc

    else:                            #crash distance

        t_stop = t_2 + v_2/(a_2)     #time since start that agent2 stops completely
        if tau<t_stop:        #agent2 passes agent1 before stopping
            d_2 = v_2*tau - 0.5*a_2*(tau-t_2)**2
            d_crash=d_2
        else:                   #agent2 stops before reachign agent1
            d_2 = v_2*t_2 + v_2**2/(2*a_2)
            d_1 = 0 #TODO
            d_crash=d_2 + d_1
        return d_crash








def d_straightInInitialHeading(th):
    global v_1, v_2, w_1, w_2, t_1, t_2, omega, g, b_brake, a_2
    if th<0:
        th=-th

    tau = t_1 + (w_1 + w_2)/(2*v_1*abs(math.sin(th))) 


    d_2 = v_2*tau
    if b_brake and t_2<tau:
        t_stop = t_2 + v_2/(a_2)     #time since start that agent2 stops completely
        if tau<t_stop:        #agent2 passes agent1 before stopping
            d_2 = v_2*tau    - 0.5*a_2*(tau**2 -  t_2**2)
        else:                   #agent2 stops before reachign agent1
            d_2 = v_2*t_stop - 0.5*a_2*(t_stop**2 - t_2**2)
        d_esc=d_2
 


    d_1 = v_1*(tau-t_1)*math.cos(th)
    d_esc = d_2 + d_1
    
    #matplotlib doesnt like having huge or -ve radii present so replace them with nans to draw
    if abs(d_esc)>100:
        d_esc = np.nan
    if (d_esc<0):
        d_esc=np.nan
    return d_esc




def d_turnOnSpotFromMoving(th):
    global v_1, v_2, w_1, w_2, t_1, t_2, omega, g, b_brake, a_2
    if th<0:
        th=-th
 
    tau = t_1 + (abs(math.pi/2 - abs(th)))/omega + (w_1/2 + w_2/2 - v_1*t_1*math.sin(th))/v_1

    d_2 = v_2*tau
    if b_brake and t_2<tau:
        t_stop = t_2 + v_2/(a_2)     #time since start that agent2 stops completely
        if tau<t_stop:        #agent2 passes agent1 before stopping
            d_2 = v_2*tau    - 0.5*a_2*(tau**2 -  t_2**2)
        else:                   #agent2 stops before reachign agent1
            d_2 = v_2*t_stop - 0.5*a_2*(t_stop**2 - t_2**2)
        d_esc=d_2
 




    d_1 = v_1*(tau-t_1)*math.cos(th)
    d_esc = d_2 + d_1

    #matplotlib doesnt like having huge or -ve radii present so replace them with nans to draw
    if abs(d_esc)>100:
        d_esc = np.nan
    if (d_esc<0):
        d_esc=np.nan
    return d_esc



def d_twist(th):
    global v_1, v_2, w_1, w_2, t_1, t_2, omega, g, b_brake, a_2

    a_2 = -1.0

    if th<0:
        th=-th
    b_flip=False
    if th>math.pi/2:
        th=math.pi-th
        b_flip=True

    R = v_1/omega
    A = math.cos(th) - (w_1+w_2)/(2*R) 

    if -1.<A<1.:
        phi = math.acos( math.cos(th) - (w_1+w_2)/(2*R) ) - th
    else:
        return np.NaN

    tau = t_1 + phi/omega

    d_2 = v_2*tau
    if b_brake and t_2<tau:


        pdb.set_trace

        t_stop = t_2 + v_2/(a_2)     #time since start that agent2 stops completely
        if tau<t_stop:        #agent2 passes agent1 before stopping
            d_2 = v_2*tau    - 0.5*a_2*(tau**2 - t_2**2 )
        else:                   #agent2 stops before reachign agent1
            d_2 = v_2*t_stop - 0.5*a_2*(t_stop**2 - t_2**2 )
    




    d_1 = R*( math.sin(th+phi) - math.sin(th))
    d_esc = d_2 + d_1
#    if b_flip:
#        d_esc = d_2 - d_1

    d_esc=d_1
    return d_esc





global v_1, v_2, w_1, w_2, t_1, t_2, omega, g, b_brake, a_2
w_1 = 0.9       #to fit intimate 0.45
v_1 = 1.4       #typical walk speed
t_1 = 1.0
a_2 = 5     #opposite sign from sim. 5 is emergency stop for a car, assume pedetrians similar.
#(sim: .9, 1.4, 1.0, 5)



w_2 = w_1       
v_2 = v_1   
t_2 = t_1

omega = math.pi/2  #rad/s 



b_brake=False

fig, ax = plt.subplots(subplot_kw={'projection': 'polar'})
xT=plt.xticks()[0]
xL=['0',r'$\frac{\pi}{4}$',r'$\frac{\pi}{2}$',r'$\frac{3\pi}{4}$',\
    r'$\pi$',r'-$\frac{3\pi}{4}$',r'$-\frac{\pi}{2}$',r'$-\frac{\pi}{4}$']
plt.xticks(xT, xL)
ax.set_theta_zero_location("N")
ax.grid(True)
ths = np.arange(-math.pi,math.pi, .001)



##CHOOSE THE STRATREGY HERE ***
f=d_instantTurnOnSpot
#f = d_turnOnSpotThenStraight
#f = d_straightInInitialHeading 
#f=d_turnOnSpotFromMoving
#f=d_twist




rs  =  list(map(f, ths))
ax.plot(ths, rs, 'k')

b_brake = True
rs  =  list(map(f, ths))
ax.plot(ths, rs, 'r')

#ax.set_rmax(2)
#ax.set_rticks([0.5, 1, 1.5, 2])  # Less radial ticks
#ax.set_rlabel_position(-22.5)  # Move radial labels away from plotted line
#ax.set_title("social zone outer", va='bottom')
#fn_png = "ana.png"
#plt.savefig(fn_png)                         
plt.show()
