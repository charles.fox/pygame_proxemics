# pygame_proxemics

Python simulation generating proxemic zones for pedestrian-pedestrian and pedestrian.car interactions of various geometries and strategies.

To run a sim,

apt install python3-pygame-zero
python3 go.py

This first simulated, animates, and logs the interactions for several minutes.  It then generates result.png showing the zones.

The intended way to run sims is to make copies of go.py and sim.py in experiments/myexp/ and run in there.   This helps keep track of all experiments you have run.

To edit a simulation, make a copy as a above then edit the sim.py file as required.   For the pedestrian, there are various strategies already avaiolable such as ped_move_twist0.  You can call any of these from update(), or add your own to call. 


## 

