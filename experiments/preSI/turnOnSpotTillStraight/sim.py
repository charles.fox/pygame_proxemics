#!/usr/bin/python3

#this is the proxemic sim, using pygame
#it prints its results to stdout
#so pipe them to a file if you want to store them somewhere
#eg
# ./sim.py > out_vcar2_vped2_twist0 
#
# you probably dont want to run the above directly, use go.py instead, which also draws the graphs at the end.
#
#edit sim.py to make variations on the basic sim as needed, and store them in experiments/

from pgzero import *
import pgzrun 
import pygame
import math, sys, pdb


global v_1, v_2, w_1, w_2, t_1, t_2, omega, g, b_brake, a_2


w_1 = 0.9       #to fit intimate 0.45
v_1 = 1.4       #typical walk speed
t_1 = 1.0
a_2 = 5.0     #opposite sign from sim. 5 is emergency stop for a car, assume pedetrians similar.

omega =  57.29   #=1rad/s in degrees (this sim uses degrees)

w_2 = w_1
v_2 = v_1
t_2 = t_1


#read arg whether to make car yield (brake)
b_brake = sys.argv[1]=="True"



##################################################################
#CF mods to pygame to make it easier and more accurate for simulations

#scratch-style sprite pixel accurate collisions, rather than rectangle hitbox collisions
def collide_pixels(actor1, actor2):
    for a in [actor1, actor2]:
        if not hasattr(a, 'mask'):
            a.mask = pygame.mask.from_surface(images.load(a.image))
        if not actor1.colliderect(actor2):
            return None
    # Offsets based on current positions of actors
    xoffset = int(actor2.left - actor1.left)
    yoffset = int(actor2.top - actor1.top)
    return actor1.mask.overlap(actor2.mask, (xoffset, yoffset))

#pygame works using pixel coordinates. These functions convert from and to SI units (meters, seconds)
WIDTH = 1400    #screen size, in pixels
HEIGHT = 600
pixpermeterx = 100.   #so 1pixel = 1cm.  Use cm as the main representation 
pixpermetery = -100.  #flipping y axis
center_x_meters = 0   #center of screen, in meters
center_y_meters = 0
ticks_per_sec = 50.
def m2p(mx, my): #meters x,y
    px = (mx-center_x_meters)*pixpermeterx + WIDTH/2     #pixel col
    py = (my-center_y_meters)*pixpermetery + HEIGHT/2     #pixel row
    return (px,py)
def p2m(px, py):  #pix row, col
    mx = ((px-WIDTH/2)/pixpermeterx)+center_x_meters
    my = ((py-HEIGHT/2)/pixpermetery)+center_y_meters
    return (mx,my)
def tick2sec(ticks):
    return ticks/ticks_per_sec
def sec2tick(sec):
    return ticks*ticks_per_sec

#these functions allow scratch/turtle like control that works with the pixel collisions  
def fwd(actor, dt, v):
    (mx,my) = p2m(actor.x, actor.y)   #pix to meters
    my += math.sin(3.14*actor.angle/180.) * (v*dt)
    mx += math.cos(3.14*actor.angle/180.) * (v*dt)
    (actor.x, actor.y) = m2p(mx, my)  #meters to pix
def turn(actor, dt, omega):  #omega is degs/sec      
    dth = dt*omega
    actor.angle += dth
    actor.mask = pygame.mask.from_surface(actor._surf) #recreate rotated mask for collisions
def drift(actor, dt, v, phi):  #phi is the desired world angle to drift at, without changing heading, eg sidestep, in rads
    (mx,my) = p2m(actor.x, actor.y)   #pix to meters
    my += math.sin(phi) * (v*dt)
    mx += math.cos(phi) * (v*dt)
    (actor.x, actor.y) = m2p(mx, my)  #meters to pix  
################################################



#this class manages running and starting many different simulations in a bisection search for d
class SimManager:
    def __init__(self):
        global sim
        global ready
        self.jobparams=[]
                

        for th in [0, 0,  10., 20.,  30., 40, 45, 50, 60, 70, 80, 90, 100, 110, 120, 130, 135, 140, 150, 160, 170, 180, 180  ]: #deg
            self.jobparams.append([th, 0.])  #dummy startdist=0m (always hit)

        self.jobID=0
        sim=Sim(self.jobparams[self.jobID])
        self.initSearch()
        ready=True

    def initSearch(self):
        self.lowerBound=0.    #somewhere that collides
        self.upperBound=64.   #somewhere that escapes
        self.mid = 64.        #latest place we have looked
        self.result = "ESCAPE"   #pretend we just ran a dist=0 sim and got collide

    def nextSim(self):
        global sim
        resolution = .01
        if abs(self.upperBound-self.lowerBound)<resolution:
            self.jobID+=1
            #quit if no more jobs to do
            if self.jobID >= len(self.jobparams):
                pygame.QUIT()
            self.initSearch()
            
        #here we have not finished a search, so make a new sim which is a step in the search
        params = self.jobparams[self.jobID]
        if self.result=="COLLIDE":   #dist needs to be longer
            self.lowerBound = self.mid
            mid_new = 0.5*(self.lowerBound+self.upperBound)
            self.mid = mid_new
        elif self.result=="ESCAPE":  #dist needs to be shorter
            self.upperBound = self.mid
            mid_new = 0.5*(self.lowerBound+self.upperBound)
            self.mid = mid_new

        #we need to crate a new sim with same jobID params as current, but overwrite the startdist
        params = self.jobparams[self.jobID]
        params[1] = self.mid
        sim=Sim(params)

class Sim:
    def __init__(self, params):    #jobID is which version of sim to run
        self.params=params
        (th, startDistance) = params

        self.ped = Actor('pedestrian')   #100x100 pix = 1mx1m
        self.ped.pos = m2p(0, 0.)
        self.ped.angle =   th   
        self.ped.mask = pygame.mask.from_surface(self.ped._surf)  #will need to redo mask on every rotate  
        self.car = Actor('pedestrian')   #400x200 pix = 4mx2m
        self.car.pos = m2p(-startDistance, 0)
        self.car.angle = 0 
        #self.car._surf = pygame.transform.scale(self.car._surf, m2p(1, -1) )
        self.car.mask = pygame.mask.from_surface(self.car._surf)
        self.ticks=0

        self.v_car = v_2  #needed for braking to allow speed to change and track it


#define many different strategies for the ped here 

def ped_move_instant_turn_on_spot(sim,dt):
    global v_1, v_2, w_1, w_2, t_1, t_2, omega, g, b_brake, a_2
    
    #wait for thinking time
    t = tick2sec(sim.ticks)   #time since start in secs
    if t<t_1:
        return 

    th_target = 90  
    sim.ped.angle = th_target
    fwd(sim.ped, dt, v_1)
 

def ped_move_straightLine_inStartTheta(sim,dt):
    global v_1, v_2, w_1, w_2, t_1, t_2, omega, g, b_brake, a_2
    #wait for thinking time
    t = tick2sec(sim.ticks)   #time since start in secs
    if t<t_1:
        return 

    fwd(sim.ped, dt, v_1)

#turn on the spot until facing 90 degrees, then walk straight forward
def ped_move_turn_on_spot(sim, dt):
    global v_1, v_2, w_1, w_2, t_1, t_2, omega, g, b_brake, a_2
    
    #wait for thinking time
    t = tick2sec(sim.ticks)   #time since start in secs
    if t<t_1:
        return 

    th_target = 90  #keep twisting until movign orthogonal to the car
    th = sim.ped.angle
    dth = th_target-th
    om=0
    if dth>1.:
        om=omega
        turn(sim.ped, dt, om)
    elif dth<-1.:
        om=-omega
        turn(sim.ped, dt, om)
    else:
        fwd(sim.ped, dt, v_1)

#walk around a great circle with a single twist. using diff directions for >/< 90 degrees
def ped_move_twist(sim, dt):

    global v_1, v_2, w_1, w_2, t_1, t_2, omega, g, b_brake, a_2
    
    #wait for thinking time
    t = tick2sec(sim.ticks)   #time since start in secs
    if t<t_1:
        return 

    fwd(sim.ped, dt, v_1)
    if sim.params[0]<=90:    
        om=omega
    else:
        om=-omega
    turn(sim.ped, dt, om)
 
def ped_move_twist_till_orthogonal(sim, dt):

    global v_1, v_2, w_1, w_2, t_1, t_2, omega, g, b_brake, a_2
    
    #wait for thinking time
    t = tick2sec(sim.ticks)   #time since start in secs
    if t<t_1:
        return 

    fwd(sim.ped, dt, v_1)
    th_target = 90  #keep twisting until movign orthogonal to the car
    th = sim.ped.angle
    dth = th_target-th
    om=0
    if dth>0:
        om=omega
    if dth<0:
        om=-omega
    turn(sim.ped, dt, om)


def ped_move_sidebacksteps(sim,dt):
    global v_1, v_2, w_1, w_2, t_1, t_2, omega, g, b_brake, a_2
    
    #wait for thinking time
    t = tick2sec(sim.ticks)   #time since start in secs
    if t<t_1:
        return  

    dth = (3.14*sim.ped.angle/180)- math.pi/2     #current heading vs desired 
    while dth>math.pi:
        dth-=(2*math.pi)
    while dth<-math.pi:
        dth+=2*math.pi  

    v =  1.1-(abs(dth)/math.pi)/2    #speed -- assuming a model where it slows as direction differs from fwd
    drift(sim.ped, dt, v, math.pi/2)  #always stepping orthogonal to car path





#strategies for Agent2 (car)
def car_move_constspeed(sim, dt):
    global v_1, v_2, w_1, w_2, t_1, t_2, omega, g, b_brake, a_2
    fwd(sim.car, dt, sim.v_car)

def car_move_braking(sim,dt):
    global v_1, v_2, w_1, w_2, t_1, t_2, omega, g, b_brake, a_2
     #wait for thinking time
    t = tick2sec(sim.ticks)   #time since start in secs
    if t<t_2:
        fwd(sim.car, dt, sim.v_car)   #keep driving at init speed
        return 

    dv = -a_2*dt
    sim.v_car += dv
    if sim.v_car<0:
        sim.v_car=0
    fwd(sim.car, dt, sim.v_car)




def update():
    global sim
    global simManager
    global ready
    global b_brake
    if not ready:
        return
    sim.ticks+=1
    dt = tick2sec(1)

    #choose which car strategy to call here
    if b_brake:
        car_move_braking(sim, dt)
    else:
        car_move_constspeed(sim, dt)

    #choose which ped strategy to call here
    #ped_move_instant_turn_on_spot(sim, dt)
    #ped_move_straightLine_inStartTheta(sim,dt)
    ped_move_turn_on_spot(sim, dt)
    #ped_move_twist(sim, dt)
    #ped_move_twist_till_orthogonal(sim, dt)
    #ped_move_sidebacksteps(sim,dt)


    if collide_pixels(sim.car, sim.ped):
        simManager.result = "COLLIDE"
        print([simManager.jobID, sim.params, simManager.result])
        simManager.nextSim()
    if sim.ticks>250:    #was 150
        simManager.result="ESCAPE"  
        print([simManager.jobID, sim.params, simManager.result])
        simManager.nextSim()

def draw():
    screen.fill((0, 50, 0)) 
    screen.draw.filled_rect( Rect(  m2p(-10, 1), m2p(10,  1) ), color=(0, 0, 0))    #black road
    #draw 1m grid
    for r in [-2, -1, 0, 1, 2]:
        for c in [-2, -1, 0, 1, 2]:
            screen.draw.filled_rect( Rect(  m2p(r, c), (4,4) ), color=(255, 255, 255))
    sim.ped.draw()
    sim.car.draw()



global sim
global simManager
global ready
ready = False
simManager = SimManager()
pgzrun.go()
