import numpy as np
import matplotlib.pyplot as plt

pi=3.142

def fr(th):
    while th>pi:
        th-=(2*pi)
    while th<-pi:
        th+=2*pi
    return 2 - (abs(th)/pi)


theta= np.arange(0, 2*3.142, 0.1)  
r = list(map(fr, theta))
fig, ax = plt.subplots(subplot_kw={'projection': 'polar'})
ax.plot(theta, r, 'b')

  
plt.show()
