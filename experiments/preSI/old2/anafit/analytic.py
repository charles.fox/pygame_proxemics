#!/usr/bin/python3


#fitting ana params

import re,sys,pdb,math, subprocess
import numpy as np
import matplotlib.pyplot as plt


def d_esc_instantTurnOnSpot(th):
    global v_1, v_2, w_1, w_2, t_1, t_2, omega, g, b_brake, a_2
    tau = t_1 + (w_1+w_2)/(2*v_1)

    d_esc = v_2*tau
    if b_brake and tau>t_2:
        d_esc  = v_2*tau + 0.5*a_2*(tau-t_2)**2
    return d_esc





def d_esc_turnOnSpotThenStraight(th):
    global v_1, v_2, w_1, w_2, t_1, t_2, omega, g, b_brake, a_2
    if th<0:
        th=-th

    tau = t_1 + (w_1 + w_2)/(2*v_1) + (abs(math.pi/2 - abs(th)))/omega 

    d_esc = v_2*tau
    if b_brake and tau>t_2:
        d_esc  = v_2*tau + 0.5*a_2*(tau-t_2)**2
    return d_esc


def d_esc_straightInInitialHeading(th):
    global v_1, v_2, w_1, w_2, t_1, t_2, omega, g, b_brake, a_2
    if th<0:
        th=-th

    #tau = t_1 + (w_1 + w_2)/(2*v_1*1) 
    tau = t_1 + (w_1 + w_2)/(2*v_1*abs(math.sin(th))) 

    d_2 = v_2*tau
    if b_brake and tau>t_2:
        d_2  = v_2*tau + 0.5*a_2*(tau-t_2)**2


    d_1 = v_1*(tau-t_1)*math.cos(th)

    d_esc = d_2 + d_1
    
    #matplotlib doesnt like having huge radii present so replace them with nans to draw
    if d_esc>100:
        d_esc = np.nan
    return d_esc




def d_esc_turnOnSpotFromMoving(th):
    global v_1, v_2, w_1, w_2, t_1, t_2, omega, g, b_brake, a_2
    if th<0:
        th=-th
 
    tau = t_1 + (abs(math.pi/2 - abs(th)))/omega + (w_1/2 + w_2/2 - v_1*t_1*math.sin(th))/v_1

    d_2 = v_2*tau
    if b_brake and tau>t_2:
        d_2  = v_2*tau + 0.5*a_2*(tau-t_2)**2

    d_1 = v_1*(tau-t_1)*math.cos(th)

    d_esc = d_2 + d_1

    return d_esc




#using maths from old version
def d_esc_twist_old(th):
    global v_1, v_2, w_1, w_2, t_1, t_2, omega, g

    if th<0:
        return d_esc_twist(-th)

    r = v_1/omega
    a = ( math.cos(th) - w_1/r  )

    d_esc=0
    if -1.<a<1.:
        phi = math.pi/2 - math.asin(a)+th
        d_esc = (v_2*t_1) + v_2*phi/omega
    return d_esc


#using new derivation
def d_esc_twist(th):
    global v_1, v_2, w_1, w_2, t_1, t_2, omega, g, b_brake, a_2

    a_2 = -1.0

    if th<0:
        th=-th
    b_flip=False
    if th>math.pi/2:
        th=math.pi-th
        b_flip=True

    R = v_1/omega
    A = math.cos(th) - (w_1+w_2)/(2*R) 

    if -1.<A<1.:
        phi = math.acos( math.cos(th) - (w_1+w_2)/(2*R) ) - th
    else:
        return np.NaN

    tau = t_1 + phi/omega
    
    d_2 = v_2*tau
    if b_brake and tau>t_2:
        d_2  = v_2*tau + 0.5*a_2*(tau-t_2)**2

    d_1 = R*( math.sin(th+phi) - math.sin(th))

    d_esc = d_2 + d_1
    if b_flip:
        d_esc = d_2 - d_1

    return d_esc





global v_1, v_2, w_1, w_2, t_1, t_2, omega, g, b_brake, a_2
w_1 = 0.9       #to fit intimate 0.45
w_2 = 0.9       

v_1 = 1.4       #typical walk speed
v_2 = 1.4   
t_1 = .25
t_2 = .25

omega = math.pi/2  #rad/s 
a_2 = 10.0


#(1.1,8   or 1.4,10)

#w_1 = 0.9       #to fit intimate 0.45
#w_2 = 0.9       
#v_1 = 1.4       #typical walk speed
#v_2 = 1.4   
#t_1 = 0.25    
#t_2 = 0.25   
#omega = math.pi/2  #rad/s 
#a_2 = 10.0



b_brake=False

fig, ax = plt.subplots(subplot_kw={'projection': 'polar'})
xT=plt.xticks()[0]
xL=['0',r'$\frac{\pi}{4}$',r'$\frac{\pi}{2}$',r'$\frac{3\pi}{4}$',\
    r'$\pi$',r'-$\frac{3\pi}{4}$',r'$-\frac{\pi}{2}$',r'$-\frac{\pi}{4}$']
plt.xticks(xT, xL)
ax.set_theta_zero_location("N")
ax.grid(True)
ths = np.arange(-math.pi,math.pi, .001)



##CHOOSE THE STRATREGY HERE ***
f=d_esc_instantTurnOnSpot
#f = d_esc_turnOnSpotThenStraight
#f = d_esc_straightInInitialHeading 
#f=d_esc_turnOnSpotFromMoving
#f=d_esc_twist




rs  =  list(map(f, ths))
ax.plot(ths, rs, 'k')

b_brake = True
rs  =  list(map(f, ths))
ax.plot(ths, rs, 'k')

#ax.set_rmax(2)
#ax.set_rticks([0.5, 1, 1.5, 2])  # Less radial ticks
#ax.set_rlabel_position(-22.5)  # Move radial labels away from plotted line
#ax.set_title("social zone outer", va='bottom')
#fn_png = "ana.png"
#plt.savefig(fn_png)                         
plt.show()
