#!/usr/bin/python3

#this is the proxemic sim, using pygame
#it prints its results to stdout
#so pipe them to a file if you want to store them somewhere
#eg
# ./sim.py > out_vcar2_vped2_twist0 
#
# you probably dont want to run the above directly, use go.py instead, which also draws the graphs at the end.
#
#edit sim.py to make variations on the basic sim as needed, and store them in experiments/

import pgzrun 
import pygame
import math, sys, pdb

w_1 = 0.9       #to fit intimate 0.4
t_1 = 0.0    #just adds a const v_2t_2 to both zone lengths   if t_2=t_1
v_1 = 1.4       #typical walk speed
a_2 = 10.0     #opposite sign from sim. 5 is emergency stop for a car, assume pedetrians similar.
omega =  1.0   #rad/s 

w_2 = w_1
v_2 = v_1
t_2 = t_1

#read arg whether to make car yield (brake)
b_brake = sys.argv[1]=="True"



class Agent:
    def __init__(self, xm, ym, thrad):
        self.xm=xm
        self.ym=ym
        self.thrad=thrad




##################################################################
#CF mods to pygame to make it easier and more accurate for simulations

#pixel accurate collisions
#def collide_pixels(actor1, actor2):
#    for a in [actor1, actor2]:
#        if not hasattr(a, 'mask'):
#            a.mask = pygame.mask.from_surface(images.load(a.image)) 
#    if not actor1.colliderect(actor2):
#        return None
#    xoffset = int(actor2.left - actor1.left)
#    yoffset = int(actor2.top - actor1.top)
#    return actor1.mask.overlap(actor2.mask, (xoffset, yoffset))

    #find hypotenuse dist between the actor centers
    #if this is < (w1+w2)/2 then collide

#def collide_pixels(actor1, actor2):
#    dxp = actor1.pos[0] - actor2.pos[0]
#    dyp = actor1.pos[1] - actor2.pos[1]
#    hp = math.sqrt(dxp**2 + dyp**2)
#    hm = hp/pixpermeterx
#    return (hm< (w_1+w_2)/2 )


def collide_pixels(agent1, agent2):
    dxm = agent1.xm - agent2.xm   
    dym = agent1.ym - agent2.ym
    hm = math.sqrt(dxm**2 + dym**2)
    return (hm< (w_1+w_2)/2 )   #assumes both circular


#pygame works using pixel coordinates. These functions convert from and to SI units (meters, seconds)
WIDTH = 1400    #screen size, in pixels
HEIGHT = 600
pixpermeterx = 100.   #so 1pixel = 1cm.  Use cm as the main representation 
pixpermetery = -100.  #flipping y axis
center_x_meters = 0   #center of screen, in meters
center_y_meters = 0
ticks_per_sec = 50.
def m2p(mx, my): #meters x,y
    px = (mx-center_x_meters)*pixpermeterx + WIDTH/2     #pixel col
    py = (my-center_y_meters)*pixpermetery + HEIGHT/2     #pixel row
    return (px,py)
def p2m(px, py):  #pix row, col
    mx = ((px-WIDTH/2)/pixpermeterx)+center_x_meters
    my = ((py-HEIGHT/2)/pixpermetery)+center_y_meters
    return (mx,my)
def tick2sec(ticks):
    return ticks/ticks_per_sec
def sec2tick(sec):
    return ticks*ticks_per_sec
def rad2deg(rad):
    return 180.*rad/math.pi
def deg2rad(deg):
    return math.pi*deg/180.


#these functions allow scratch/turtle like control that works with the pixel collisions  
def fwd(agent, dt, v):
    agent.xm += math.cos(agent.thrad) * (v*dt)
    agent.ym += math.sin(agent.thrad) * (v*dt)
def turn(agent, dt, omega):  #omega is rad/sec      
    agent.thrad += dt*omega
#    actor.mask = pygame.mask.from_surface(actor._surf) #recreate rotated mask for collisions
def drift(actor, dt, v, phi):  #phi is the desired world angle to drift at, in rads
    agent.ym += math.sin(phi) * (v*dt)
    agent.xm += math.cos(phi) * (v*dt)
################################################



#this class manages running and starting many different simulations in a bisection search for d
class SimManager:
    def __init__(self):
        global sim
        global ready
        self.jobparams=[]
        for th in [0, 0, math.pi/16,  2*math.pi/16, 3*math.pi/16, 4*math.pi/15, 5*math.pi/16, 6*math.pi/16, 7*math.pi/16, 8*math.pi/16, 9*math.pi/16, 10*math.pi/16, 11*math.pi/16, 12*math.pi/16, 13*math.pi/16, 14*math.pi/16, 15*math.pi/16, math.pi, math.pi   ]: #rad
            self.jobparams.append([th, 0.])  #dummy startdist=0m (always hit)
        self.jobID=0
        sim=Sim(self.jobparams[self.jobID])
        self.initSearch()
        ready=True

    def initSearch(self):
        self.lowerBound=0.    #somewhere known a priori to collide
        self.upperBound=64.   #somewhere known a priori to escape
        self.mid = self.lowerBound        #latest place we have looked
        self.result = "COLLIDE"   #pretend we just ran a dist=0 sim and got collide

    def nextSim(self):
        global sim
        resolution = .01
        if abs(self.upperBound-self.lowerBound)<resolution:

    #        while 1:
    #            pdb.set_trace()

            self.jobID+=1
            if self.jobID >= len(self.jobparams):            #quit if no more jobs
                pygame.QUIT()
            self.initSearch()
            
        #here we have not finished a search, so make a new sim which is a step in the search
        params = self.jobparams[self.jobID]
 #       pdb.set_trace()
        if self.result=="COLLIDE":   #dist needs to be longer
            self.lowerBound = self.mid
            mid_new = 0.5*(self.lowerBound+self.upperBound)
            self.mid = mid_new
        elif self.result=="ESCAPE":  #dist needs to be shorter
            self.upperBound = self.mid
            mid_new = 0.5*(self.lowerBound+self.upperBound)
            self.mid = mid_new

        #we need to crate a new sim with same jobID params as current, but overwrite the startdist
        params = self.jobparams[self.jobID]
        params[1] = self.mid
        sim=Sim(params)

class Sim:
    def __init__(self, params):    #jobID is which version of sim to run
        self.params=params
        (th, startDistance) = params

        self.ped = Agent(0., 0., th)
        self.car = Agent(-startDistance, 0., 0.)
        self.car.vm = v_2   #init car speed

        self.aped = Actor('pedestrian')   #90x90 pix = 0.9x0.9m
        self.acar = Actor('pedestrian')   #90x90pix
 
        self.ticks=0


#define many different strategies for the ped here 

def ped_move_instant_turn_on_spot(sim,dt):
    global v_1, v_2, w_1, w_2, t_1, t_2, omega, g, b_brake, a_2
    
    t = tick2sec(sim.ticks)   #time since start in secs
    if t<t_1:     #thinking time, do nothing
        return 

    th_target = math.pi/2  
    sim.ped.thrad = th_target    #instant change angle to escape orthogonally
    fwd(sim.ped, dt, v_1)        #walk straight forward
 

def ped_move_straightLine_inStartTheta(sim,dt):
    global v_1, v_2, w_1, w_2, t_1, t_2, omega, g, b_brake, a_2
    t = tick2sec(sim.ticks)   #time since start in secs
    if t<t_1:                  #think time
        return 
    fwd(sim.ped, dt, v_1)

#turn on the spot until facing 90 degrees, then walk straight forward
def ped_move_turn_on_spot(sim, dt):
    global v_1, v_2, w_1, w_2, t_1, t_2, omega, g, b_brake, a_2
    
    #wait for thinking time
    t = tick2sec(sim.ticks)   #time since start in secs
    if t<t_1:
        return 

    th_target = math.pi/2  
    sim.ped.thrad = th_target    #instant change angle to escape orthogonally
    dth = th_target-th
    om=0
    if dth>1.:
        om=omega
        turn(sim.ped, dt, om)
    elif dth<-1.:
        om=-omega
        turn(sim.ped, dt, om)
    else:
        fwd(sim.ped, dt, v_1)

#walk around a great circle with a single twist. using diff directions for >/< 90 degrees
def ped_move_twist(sim, dt):
    global v_1, v_2, w_1, w_2, t_1, t_2, omega, g, b_brake, a_2
    
    #wait for thinking time
    t = tick2sec(sim.ticks)   #time since start in secs
    if t<t_1:
        return 

    fwd(sim.ped, dt, v_1)
    if sim.params[0]<=math.pi/2:    
        om=omega
    else:
        om=-omega
    turn(sim.ped, dt, om)
 
def ped_move_twist_till_orthogonal(sim, dt):
    global v_1, v_2, w_1, w_2, t_1, t_2, omega, g, b_brake, a_2
    
    #wait for thinking time
    t = tick2sec(sim.ticks)   #time since start in secs
    if t<t_1:
        return 

    fwd(sim.ped, dt, v_1)

    th_target = math.pi/2  
    
    dth = th_target-sim.ped.thrad
    om=0
    if dth>0:
        om=omega
    if dth<0:
        om=-omega
    turn(sim.ped, dt, om)


def ped_move_sidebacksteps(sim,dt):
    global v_1, v_2, w_1, w_2, t_1, t_2, omega, g, b_brake, a_2
    
    #wait for thinking time
    t = tick2sec(sim.ticks)   #time since start in secs
    if t<t_1:
        return  

    dth = sim.ped.thrad- math.pi/2     #current heading vs desired 
    while dth>math.pi:
        dth-=(2*math.pi)
    while dth<-math.pi:
        dth+=2*math.pi  

    v =  1.1-(abs(dth)/math.pi)/2     #speed -- assuming a model where it slows as direction differs from fwd
    drift(sim.ped, dt, v, math.pi/2)  #always stepping orthogonal to car path





#strategies for Agent2 (car)
def car_move_constspeed(sim, dt):
    global v_1, v_2, w_1, w_2, t_1, t_2, omega, g, b_brake, a_2
    fwd(sim.car, dt, sim.car.vm)

def car_move_braking(sim,dt):
    global v_1, v_2, w_1, w_2, t_1, t_2, omega, g, b_brake, a_2
     #wait for thinking time
    t = tick2sec(sim.ticks)   #time since start in secs
    if t<t_2:
        fwd(sim.car, dt, sim.car.vm)   #keep driving at init speed
        return 

    dv = -a_2*dt
    sim.car.vm += dv
    if sim.car.vm<0:
        sim.car.vm=0
    fwd(sim.car, dt, sim.car.vm)




def update():
    global v_1, v_2, w_1, w_2, t_1, t_2, omega, g, b_brake, a_2
    global sim, simManager, ready
    if not ready:
        return
    sim.ticks+=1
    dt = tick2sec(1)       #length of one tick in seconds

    #choose which car strategy to call here
    if b_brake:
        car_move_braking(sim, dt)
    else:
        car_move_constspeed(sim, dt)

    #choose which ped strategy to call here
    #ped_move_instant_turn_on_spot(sim, dt)
    #ped_move_straightLine_inStartTheta(sim,dt)
    #ped_move_turn_on_spot(sim, dt)
    #ped_move_twist(sim, dt)
    ped_move_twist_till_orthogonal(sim, dt)
    #ped_move_sidebacksteps(sim,dt)



#######    print([sim.ticks, sim.car.xm, sim.car.ym, sim.ped.xm, sim.ped.ym])

    if collide_pixels(sim.car, sim.ped):
        simManager.result = "COLLIDE"
        print([simManager.jobID, sim.params, simManager.result])
        simManager.nextSim()
        
    if sim.ped.ym > (w_1+w_2)/2:   #as soon as leaves the corridor for 1st time
        simManager.result="ESCAPE"  
        print([simManager.jobID, sim.params, simManager.result])
#        print([sim.ticks, tick2sec(sim.ticks)])
        simManager.nextSim()

def draw():
    screen.fill((0, 50, 0))  #green background


    screen.draw.filled_rect( Rect(  m2p(-10, w_2/2),      tuple(map( lambda x,y: x-y,   m2p(10, -w_2/2) , m2p(-10, w_2/2) ))           ), color=(0, 0, 0))    #black road
    #draw 1m grid
    for r in [-2, -1, 0, 1, 2]:
        for c in [-2, -1, 0, 1, 2]:
            screen.draw.filled_rect( Rect(  m2p(r, c), (4,4) ), color=(255, 255, 255))
    

    sim.aped.pos = m2p(sim.ped.xm, sim.ped.ym)
    sim.aped.angle = rad2deg(sim.ped.thrad)   
    sim.aped.mask = pygame.mask.from_surface(sim.aped._surf)  #will need to redo mask on every rotate  
    sim.aped.draw()

    sim.acar.pos = m2p(sim.car.xm, sim.car.ym)
    sim.acar.angle = rad2deg(sim.car.thrad)   
    sim.acar.mask = pygame.mask.from_surface(sim.acar._surf)  #will need to redo mask on every rotate  
    sim.acar.draw()



ready = False
simManager = SimManager()
pgzrun.go()
