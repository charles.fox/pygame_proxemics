#!/usr/bin/python3

#fitting ana params

import re,sys,pdb,math, subprocess
import numpy as np
import matplotlib.pyplot as plt


def relu(x):
    return x if x>0 else 0

def d_instantTurnOnSpot(th):
    global v_1, v_2, w_1, w_2, t_1, t_2, omega, g, b_brake, a_2

    w = (w_1+w_2)/2   #distance between agent centers when they are first just touching

    if not b_brake:     #escape distance
        th = math.atan2(v_1, v_2)   #see geom fig

        d_1 = w*math.sin(th)
        y_1 = w*math.cos(th)
        tau = y_1/v_1    #time for agetn1 to reach collision point (NOT to escape -- collision occurs before escape)
   
        d_2 = t_1*v_2 + tau*v_2
        d_esc = d_2 + d_1

        return d_esc

    else:                            #crash distance, APPROXIMATED with corridor exit approximation

        tau = t_1 + w/v_1             #time of Agent1 exiting corridor

        d_crash = v_2 * tau           #dist travelled by a2 if not braking

        bt = tau-t_2                    #braking time available
        if bt>0:
            d_crash -= (a_2/2)*bt**2    #apply decelleration

        return d_crash




def d_turnOnSpotThenStraight(th):
    global v_1, v_2, w_1, w_2, t_1, t_2, omega, g, b_brake, a_2
    if th<0:
        th=-th

    tau = t_1 + (w_1 + w_2)/(2*v_1) + (abs(math.pi/2 - abs(th)))/omega     #time for Agent1 to leave corridor 

    if not b_brake or (t_2>tau):     #escape distance
        d_2 = v_2*tau
        d_esc = d_2
        return d_esc

    else:                            #crash distance

        t_stop = t_2 + v_2/(a_2)     #time since start that agent2 stops completely
        if tau<t_stop:        #agent2 passes agent1 before stopping
            d_2 = v_2*tau - 0.5*a_2*(tau-t_2)**2
            d_crash=d_2
        else:                   #agent2 stops before reachign agent1
            d_2 = v_2*t_2 + v_2**2/(2*a_2)
            d_1 = 0 #TODO
            d_crash=d_2 + d_1
        return d_crash








def d_straightInInitialHeading(th):
    global v_1, v_2, w_1, w_2, t_1, t_2, omega, g, b_brake, a_2
    if th<0:
        th=-th

    tau = t_1 + (w_1 + w_2)/(2*v_1*abs(math.sin(th))) 


    d_2 = v_2*tau
    if b_brake and t_2<tau:
        t_stop = t_2 + v_2/(a_2)     #time since start that agent2 stops completely
        if tau<t_stop:        #aagent1 exits corridor before agent2 stops; so agent2 is moving at corridorexit contact.
            d_2 = v_2*tau    - 0.5*a_2*(tau - t_2)**2
        else:                   #agent2 stops before agent1 exists, so agent2 is stationary at corridorexit contact.
            d_2 = v_2*t_stop - 0.5*a_2*(t_stop**2 - t_2)**2
        d_esc=d_2
 


    d_1 = v_1*(tau-t_1)*math.cos(th)
    d_esc = d_2 + d_1
    
    #matplotlib doesnt like having huge or -ve radii present so replace them with nans to draw
    if abs(d_esc)>100:
        d_esc = np.nan
    if (d_esc<0):
        d_esc=np.nan
    return d_esc




def d_turnOnSpotFromMoving(th):
    global v_1, v_2, w_1, w_2, t_1, t_2, omega, g, b_brake, a_2
    if th<0:
        th=-th
    w = (w_1+w_2)/2

    tau = t_1 + (abs(math.pi/2 - abs(th)))/omega + w/v_1 - t_1*math.sin(th)  #time for A1 to escape the corridor

    d_2 = v_2*tau
    if b_brake and t_2<tau:
        t_stop = t_2 + v_2/a_2     #time since start that agent2 stops completely
        if tau<t_stop:        #agent2 still moving when agent1 escapes
            d_2 = v_2*tau    - 0.5*a_2*(tau    - t_2)**2
        else:                   #agent2 stops before agent1 escapes
            d_2 = v_2*t_stop - 0.5*a_2*(t_stop - t_2)**2
 
    d_1 = v_1*(t_1)*math.cos(th)   #horiz component of linear motion in original heading during think time
    d_esc = d_2 + d_1



    #matplotlib doesnt like having huge or -ve radii present so replace them with nans to draw
    if abs(d_esc)>100:
        d_esc = np.nan
    if (d_esc<0):
        d_esc=np.nan
    return d_esc



def d_twist(th):
    #global v_1, v_2, w_1, w_2, t_1, t_2, omega, g, b_brake, a_2


    if th<0:
        th=-th
    b_flip=False
    
    if th>math.pi/2:
        th=math.pi-th
        b_flip=True

    R = v_1/omega

    A = math.cos(th) - (w_1+w_2)/(2*R) 
    if not -1.<A<1.:
        return np.NaN


    phi = math.acos( math.cos(th) - (w_1+w_2)/(2*R) ) - th
    tau = t_1 + phi/omega
    d_2 = v_2*tau



    if b_brake and t_2<tau:
        t_stop = t_2 + v_2/(a_2)     #time since start that agent2 stops completely
        if tau<t_stop:        #agent2 passes agent1 before stopping
            d_2 = v_2*tau    - 0.5*a_2*(tau - t_2 )**2
        else:                   #agent2 stops before reachign agent1
            d_2 = v_2*t_stop - 0.5*a_2*(t_stop - t_2 )**2
    


    d_1 = R*( math.sin(th+phi) - math.sin(th))
    d_esc = d_2 + d_1
    if b_flip:
        d_esc = d_2 - d_1


    if d_esc<0:
        d_esc=0

    return d_esc





global v_1, v_2, w_1, w_2, t_1, t_2, omega, g, b_brake, a_2

#old paper
#w_1 = 1.19         #to fit intimate 0.45
#v_1 = 1.1          #typical walk speed
#t_1 = 1.1   
#a_2 = 5            #opposite sign from sim

#new paper
w_1 = 0.9       #to fit intimate 0.45
v_1 = 1.4       #typical walk speed
t_1 = 1.0
a_2 = 5  #10.       #opposite sign from sim


w_2 = w_1       
v_2 = v_1   
t_2 = t_1

omega = 1.0 #math.pi/2  #rad/s 




fig, ax = plt.subplots(subplot_kw={'projection': 'polar'})
xT=plt.xticks()[0]
xL=['0',r'$\frac{\pi}{4}$',r'$\frac{\pi}{2}$',r'$\frac{3\pi}{4}$',\
    r'$\pi$',r'-$\frac{3\pi}{4}$',r'$-\frac{\pi}{2}$',r'$-\frac{\pi}{4}$']
plt.xticks(xT, xL)
ax.set_theta_zero_location("N")
ax.grid(True)
ths = np.arange(-math.pi,math.pi, .001)



##CHOOSE THE STRATREGY HERE ***
f=d_instantTurnOnSpot
#f = d_turnOnSpotThenStraight
#f = d_straightInInitialHeading 
#f=d_turnOnSpotFromMoving
#f=d_twist



b_brake=False
rs  =  list(map(f, ths))
ax.plot(ths, rs, 'k')

b_brake = True
rs  =  list(map(f, ths))
ax.plot(ths, rs, 'r')

#ax.set_rmax(2)
#ax.set_rticks([0.5, 1, 1.5, 2])  # Less radial ticks
#ax.set_rlabel_position(-22.5)  # Move radial labels away from plotted line
#ax.set_title("social zone outer", va='bottom')
#fn_png = "ana.png"
#plt.savefig(fn_png)                         
plt.show()
